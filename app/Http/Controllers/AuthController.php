<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register(Request $request)
    {
        $input = $request->all();

        if ($request->get('password') !== $request->get('confirm_password')) {
            return back()->withInput($input);
        }

        $input['password'] = Hash::make($input['password']);

        User::create($input);

        $request->session()->flush();

        return redirect(route('login_form'));
    }

    public function login(Request $request)
    {
        $userData = [
            'email' => $request->get('email'),
            'password' => $request->get('password')
        ];
        if (Auth::attempt($userData)) {
            return redirect(route('categories.list'));
        }

        return back();
    }
}
