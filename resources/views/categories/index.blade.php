<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Category Page</title>

        <link href={{asset('css/sb-admin-2.css')}} rel="stylesheet">
    </head>
    <body>
        <div class="card shadow mb-4">
            <div style="display: flex; justify-content: space-between" class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">Categories</h6>
                <div>
                    <a href="{{ route('logout') }}" class="btn btn-primary">Logout</a>
                    <a href="{{ route('posts.list') }}" class="btn btn-primary">Go to Posts</a>
                </div>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Name</th>
                                <th>
                                    @can('store-categories')
                                        <a href="{{ route('categories.create') }}" class="btn btn-primary">+ (New)</a>
                                    @endcan
                                </th>
                            </tr>
                        </thead>
                        
                        <tbody>
                            @foreach($categories as $category)
                                <tr>
                                    <td>{{ $loop->index + 1 }}</td>
                                    <td>{{ $category->name }}</td>
                                    <td>
                                        <div style="display: flex">
                                            @can('update-categories')
                                                <div>
                                                    <a class="btn btn-primary" style="margin-right: 3px" href="{{ route('categories.edit', $category->id) }}">Edit</a>
                                                </div>
                                            @endcan
    
                                            @can('destroy-categories')
                                                <div>
                                                    <form action="{{ route('categories.delete', $category->id) }}" method="POST">
                                                        @csrf
                                                        @method('delete')
                                                        <button type="submit" class="btn btn-danger">Delete</button>
                                                    </form>
                                                </div>
                                            @endcan
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </body>
</html>