<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Registration Page</title>

        <link href={{asset('css/general-style.css')}} rel="stylesheet">
    </head>
    <body>
        <div class="wrapper fadeInDown">
            <div id="formContent">
                <!-- Tabs Titles -->
            
                <!-- Icon -->
                <div class="fadeIn first" style="font-size: 30px; padding: 10px">
                Registration Form
                </div>
            
                <!-- Registration Form -->
                <form action="{{ route('register') }}" method="POST">
                    @csrf
                    <input type="text" id="name" class="fadeIn second" name="name" placeholder="Username">
                    <input type="text" id="email" class="fadeIn third" name="email" placeholder="Email">
                    <input type="password" id="password" class="fadeIn second" name="password" placeholder="Password">
                    <input type="password" id="confirm_password" class="fadeIn third" name="confirm_password" placeholder="Confirm Password">
                    <input type="submit" class="fadeIn fourth" value="Create">
                </form>
            </div>
        </div>
    </body>
</html>