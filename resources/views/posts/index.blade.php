<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Post Page</title>

        <link href={{asset('css/sb-admin-2.css')}} rel="stylesheet">
    </head>
    <body>
        <div class="wrapper fadeInDown">
            <div id="formContent">
                <div class="fadeIn first" style="font-size: 30px; padding: 10px">
                    <div class="card shadow mb-4">
                        <div class="card-header py-3">
                            <h6 class="m-0 font-weight-bold text-primary">Posts</h6>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Category Name</th>
                                            <th>Title</th>
                                            <th>Content</th>
                                            <th>Author</th>
                                            <th>
                                                <a href="{{ route('posts.create') }}" class="btn btn-primary">+ (New)</a>
                                            </th>
                                        </tr>
                                    </thead>
                                    
                                    <tbody>
                                        @foreach($posts as $post)
                                            <tr>
                                                <td>{{ $loop->index + 1 }}</td>
                                                <td>{{ $post->category->name }}</td>
                                                <td>{{ $post->title }}</td>
                                                <td>{{ $post->content }}</td>
                                                <td>{{ $post->author->full_name }}</td>
                                                <td>
                                                    <div style="display: flex">
                                                        @can('update-posts', $post)
                                                            <div>
                                                                <a class="btn btn-primary" style="margin-right: 3px" href="{{ route('posts.edit', $post->id) }}">Edit</a>
                                                            </div>
                                                        @endcan
                
                                                        @can('destroy-posts', $post)
                                                            <div>
                                                                <form action="{{ route('posts.destroy', $post->id) }}" method="POST">
                                                                    @csrf
                                                                    @method('delete')
                                                                    <button type="submit" class="btn btn-danger">Delete</button>
                                                                </form>
                                                            </div>
                                                        @endcan
                                                    </div>
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>