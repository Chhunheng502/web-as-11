<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Login Page</title>

        <link href={{asset('css/general-style.css')}} rel="stylesheet">
    </head>
    <body>
        <div class="wrapper fadeInDown">
            <div id="formContent">
                <!-- Tabs Titles -->
            
                <div class="fadeIn first" style="font-size: 30px; padding: 10px">
                Login Form
                </div>
            
                <!-- Login Form -->
                <form action="{{ route('login') }}" method="POST">
                    @csrf
                    <input type="text" id="email" class="fadeIn second" name="email" placeholder="Email">
                    <input type="password" id="password" class="fadeIn third" name="password" placeholder="Password">
                    <input type="submit" class="fadeIn fourth" value="Log In">
                </form>
        
                <div id="formFooter">
                <span>Don't have an account? <a class="underlineHover" href="{{ route('register_form') }}"> Register Now </a> </span>
                </div>
            </div>
        </div>
    </body>
</html>