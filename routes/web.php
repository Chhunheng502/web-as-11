<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
})->name('login_form');

Route::post('/login', [\App\Http\Controllers\AuthController::class, 'login'])->name('login');

Route::get('/logout', function () {
    return view('welcome');
})->name('logout');

Route::get('/register', function () {
    return view('register');
})->name('register_form');

Route::post('/register', [\App\Http\Controllers\AuthController::class, 'register'])->name('register');

Route::get('/categories', [\App\Http\Controllers\CategoryController::class, 'index'])->name('categories.list');
Route::get('/categories/create', [\App\Http\Controllers\CategoryController::class, 'create'])->name('categories.create');
Route::post('/categories/store', [\App\Http\Controllers\CategoryController::class, 'store'])->name('categories.store');

Route::get('/categories/{category}/edit', [\App\Http\Controllers\CategoryController::class, 'edit'])
->name('categories.edit')->middleware('can:update-categories');

Route::put('/categories/{category}/update', [\App\Http\Controllers\CategoryController::class, 'update'])->name('categories.update');
Route::delete('/categories/{category}/delete', [\App\Http\Controllers\CategoryController::class, 'destroy'])->name('categories.delete');

// .............................................................

Route::get('/posts', [\App\Http\Controllers\PostController::class, 'index'])->name('posts.list');
Route::get('/posts/create', [\App\Http\Controllers\PostController::class, 'create'])->name('posts.create');
Route::post('/posts/store', [\App\Http\Controllers\PostController::class, 'store'])->name('posts.store');

Route::get('/posts/{post}/edit', [\App\Http\Controllers\PostController::class, 'edit'])
->name('posts.edit')->middleware('can:update-posts');

Route::put('/posts/{post}/update', [\App\Http\Controllers\PostController::class, 'update'])->name('posts.update');
Route::delete('/posts/{post}/delete', [\App\Http\Controllers\PostController::class, 'destroy'])->name('posts.destroy');

// Route::group([
//     'middleware' => ['web', 'admin_role']
// ], function() {
//     Route::resource('users', \App\Http\Controllers\UserController::class); 
// });

Route::resource('users', \App\Http\Controllers\UserController::class)->middleware('admin_role'); 
